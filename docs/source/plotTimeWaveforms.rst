Plot Time Waveforms
===================

Time waveforms will be plotted in 2 ways:

* :ref:`Plot time waveforms of a station<plotTWS>` 
  
* :ref:`Plot time waveforms of a channel<plotTWC>` 
  

.. _plotTWS:

Plot time waveforms for a station
---------------------------------

Provide waveforms of all (or a subset of) channels of a station

To get more information for this command:

.. code-block:: console

   $plotTimeWaveformsS -h

To run the command:

.. code-block:: console

   $plotTimeWaveformsS SDSdirectory --station stationCode --startTime  "YYYY-MM-DDTHH:MM:SS" --endTime "YYYY-MM-DDTHH:MM:SS" [Options]

The following is the use of the plotTimeWaveformsS command with all optional arguments:

.. code-block:: console

   $plotTimeWaveformsS -h
   usage: plotTimeWaveformsS [-h] --station STACOD [--output OUTPUT]
                          [--outFormat FMTID] [--result RESULT] --startTime
                          START_TIME [--endTime END_TIME]
                          [--duration SECONDCOUNT] [--equalScale]
                          INPUTDIR

   Provide waveforms of all (or a subset of) channels of a station (fournit
   graphe/courbe de réponse instrumentale, avec tous les canaux, pour une
   station). Fournit diagramme de séries temporelles pour chaque station,
   relativement à un événement sismique particulier (Doc Olivier)

   positional arguments:
     INPUTDIR              <INPUTDIR> input file path / directory

   optional arguments:
     -h, --help            show this help message and exit
     --station STACOD      <STACOD> Required station code
     --output OUTPUT       <OUTPUT> Optional output file name
     --outFormat FMTID     <FMTID> Optional format of output file (JPEG, PNG,
                           SVG)
     --result RESULT       <RESULT> Optional path to a CSV output file
     --startTime START_TIME
                           <START_TIME> Optional starting time
     --endTime END_TIME    <END_TIME> Optional end time
     --duration SECONDCOUNT
                           <SECONDCOUNT> Optional number of second for each
                           station waveform plot, this usually corresponds to the
                           duration of a seismic event
     --equalScale          Optional equal scale, to plot all waveforms in equal
                           scale. The default value is False.

.. _plotTWC:

Plot time waveforms for a channel
---------------------------------

Create a time waveforms plot of a channel of all stations related to this channel.

To get more information for this command:

.. code-block:: console
 
   $plotTimeWaveformsC -h

To run the command:

.. code-block:: console

   $plotTimeWaveformsC SDSdirectory --channel channelCode --startTime  "YYYY-MM-DDTHH:MM:SS" --endTime "YYYY-MM-DDTHH:MM:SS" [Options]

The following is the use of the plotTimeWaveformsC command with all optional arguments:

.. code-block:: console

   $plotTimeWaveformsC -h
   usage: plotTimeWaveformsC [-h] [--station STACOD] --channel CHACOD
                          [--output OUTPUT] [--outFormat FMTID]
                          [--result RESULT] --startTime START_TIME
                          [--endTime END_TIME] [--duration SECONDCOUNT]
                          [--outUnit OUTUNIT] [--no-removeResponse]
                          [--equalScale]
                          INPUTPATH INPUTMETAFILE

   Provide waveforms of a channel for all stations (fournit graphe/courbe de
   series temporelles pour un canal de toutes les stations)

   positional arguments:
     INPUTPATH             <INPUTPATH> input SDS root directory
     INPUTMETAFILE         <INPUTMETAFILE> input file, wildcards accepted (please
                           write the name with wildcards * between quotes, ex.
                           "*.xml")

   optional arguments:
     -h, --help            show this help message and exit
     --station STACOD      <STACOD> Optional station code
     --channel CHACOD      <CHACOD> Required channel code
     --output OUTPUT       <OUTPUT> Optional output file name
     --outFormat FMTID     <FMTID> Optional format of output file (JPEG or PNG)
     --result RESULT       <RESULT> Required path to a CSV output file
     --startTime START_TIME
                           <START_TIME> Optional starting time
     --endTime END_TIME    <END_TIME> Optional end time
     --duration SECONDCOUNT
                           <SECONDCOUNT> Optional number of second for each
                           station waveform plot, this usually corresponds to the
                           duration of a seismic event
     --outUnit OUTUNIT     <OUTUNIT> Optional output unit. The valid values are:
                           DISP, VEL or ACC. The fault value is VEL
     --no-removeResponse   Optional no remove response, not to deconvolve
                           instrument response. The default value is False.
     --equalScale          Optional equal scale, to plot all waveforms in equal
                           scale. The default value is False.
