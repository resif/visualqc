Plot Data Availability
======================

This command creates a plot of all available data in a directory.
Type the following command to see more explanation.

.. code-block:: console

   $ plotDataAvailability -h

To create an image showing data availability of a directory

.. code-block:: console

   $ plotDataAvailability /path/to/directory/containing/data/

The following is the use of the plotDataAvailability command with all optional arguments:

.. code-block:: console

   $ plotDataAvailability -h
   usage: plotDataAvailability [-h] [--output OUTPUTFILE] [--outFormat FMTID]
                            [--startTime START_TIME] [--endTime END_TIME]
                            [--eventTime EVENT_TIME]
                            INPUTDIR

   Provide data availability of all stations of the network, fournir la disponibilité de données  
   des stations du réseau)

   positional arguments:
     INPUTDIR              <INPUTDIR> input file path / directory

   optional arguments:
     -h, --help            show this help message and exit
     --output OUTPUTFILE   <OUTPUTFILE> Optional output file name
     --outFormat FMTID     <FMTID> Optional format of output file (JPEG or PNG)
     --startTime START_TIME
                        <START_TIME> Optional starting time
     --endTime END_TIME    <END_TIME> Optional end time
     --eventTime EVENT_TIME
                           <EVENT_TIME> Optional event time
